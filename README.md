# K-means

This sample demonstrates the online learning capabilities of Spark MLlib.

Train and test data should be passed through Kafka topic in format:
```
(<label>,[<feature_1>,<feature_2,...>])
```

Output records will be passed through output topic in format:
+ key - same as input record (string)
+ value - predicted label 

## Configurations
```properties
#Spark master URL
spark.master.url=local[4]

#Spark application name
spark.appName=logistic-regression-with-sgd-example

#Default Kafka group ID
stream.kafka.groupId=online-learning

#List of Kafka input stream brokers (host1:port1,host2:port2,...)
stream.kafka.brokers=localhost:9092

#Input stream topics
stream.kafka.topics.testData=test_data

#Output stream topic
stream.kafka.topic.output=output

#Labeled data stream topics (for online learning)
stream.kafka.topics.trainData=train_data

#Logging level
logging.level=INFO

#Spark streaming timeout duration
spark.streaming.checkpoint=/tmp

#Spark streaming batch time in millis
spark.streaming.batchTime=1000

#Spark streaming timeout duration
spark.streaming.timeout=null

#Number of features in train/test dataset
spark.ml.numOfFeatures=null

#Number of clusters
spark.ml.kmeans.numOfClusters=null

#Weight for each center
spark.ml.kmeans.initPointWeight=100.0

# Half life time unit ("batches" or "points") for forgetful algorithms. If points, then the decay factor is raised to the power of number of new points and if batches, then decay factor will be used as is.
spark.ml.kmeans.halfLife.unit=batches

#Half life value
spark.ml.kmeans.halfLife.value=3
```