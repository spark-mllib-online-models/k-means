package hr.fer.ztel.spark.online_models.k_means.kafka

import java.util.Properties

import org.apache.kafka.clients.producer.{KafkaProducer, ProducerConfig, ProducerRecord}

/**
 * Wrapper used for broadcasting KafkaProducer object throughout Spark distributed system.
 *
 * @param createProducer factory method for producing MessageProducerServiceKafka objects
 * @tparam K key type
 * @tparam R value type
 */
class KafkaProducerWrapper[K, R](createProducer: () => (String, KafkaProducer[K, R])) extends Serializable {

  /* This is the key idea that allows us to work around running into
     NotSerializableExceptions. */
  lazy val producer: (String, KafkaProducer[K, R]) = createProducer()

  def send(key: K, data: R): Unit = {
    producer._2.send(new ProducerRecord[K, R](producer._1, key, data))
  }

}

/**
 * Factory object
 */
object KafkaProducerWrapper {

  def apply[K, R](kafkaBroker: String,
                  kafkaGroupId: String,
                  outTopic: String,
                  keySerializer: String,
                  valueSerializer: String): KafkaProducerWrapper[K, R] = {
    val createProducerFunc = () => {
      val producerConfig = new Properties()
      producerConfig.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, kafkaBroker)
      producerConfig.put(ProducerConfig.CLIENT_ID_CONFIG, kafkaGroupId)
      producerConfig.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, keySerializer)
      producerConfig.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, valueSerializer)

      val producer = new KafkaProducer[K, R](producerConfig)

      sys.addShutdownHook {
        // Ensure that, on executor JVM shutdown, the Kafka producer sends
        // any buffered messages to Kafka before shutting down.
        producer.close()
      }

      (outTopic, producer)
    }

    new KafkaProducerWrapper[K, R](createProducerFunc)
  }
}
