package hr.fer.ztel.spark.online_models.k_means.configuration

object ConfigurationFields {

  /**
   * Key for accessing the location of configuration file
   */
  val CONFIG_FILE_DEFAULT_LOCATION: String = "config.properties"

  /**
   * Key for accessing the default Kafka group ID
   */
  val STREAM_KAFKA_GROUP_ID: String = "stream.kafka.groupId"

  /**
   * Key for accessing list of Kafka input stream brokers (host1:port1,host2:port2,...)
   */
  val STREAM_KAFKA_BROKERS: String = "stream.kafka.brokers"

  /**
   * Key for accessing the input stream topics
   */
  val STREAM_KAFKA_TOPICS_TEST_DATA: String = "stream.kafka.topics.testData"

  /**
   * Key for accessing the output stream topics
   */
  val STREAM_KAFKA_TOPIC_OUTPUT: String = "stream.kafka.topic.output"

  /**
   * Key for accessing the labeled data stream topics (for online learning)
   */
  val STREAM_KAFKA_TOPICS_TRAIN_DATA: String = "stream.kafka.topics.trainData"

  /**
   * Key for accessing Sparks master URL
   */
  val SPARK_MASTER_URL: String = "spark.master.url"

  /**
   * Key for accessing the default Spark application name
   */
  val SPARK_APP_NAME: String = "spark.appName"

  /**
   * Logging level
   */
  val LOGGING_LEVEL: String = "logging.level"

  /**
   * Spark streaming timeout duration
   */
  val CHECKPOINT_DIR: String = "spark.streaming.checkpoint"

  /**
   * Spark streaming batch time
   */
  val SPARK_STREAMING_BATCH_TIME_MILLIS: String = "spark.streaming.batchTime"

  /**
   * Spark streaming timeout duration
   */
  val SPARK_STREAMING_TIMEOUT_SECONDS: String = "spark.streaming.timeout"

  /**
   * Number of features in train/test dataset
   */
  val NUMBER_OF_FEATURES: String = "spark.ml.numOfFeatures"

  /**
   * Number of clusters
   */
  val NUM_OF_CLUSTERS: String = "spark.ml.kmeans.numOfClusters"

  /**
   * Weight for each center
   */
  val RANDOM_POINT_WEIGHT :String = "spark.ml.kmeans.initPointWeight"

  /**
   * Half life value.
   */
  val HALF_LIFE_VALUE: String = "spark.ml.kmeans.halfLife.value"

  /**
   * Half life time unit ("batches" or "points") for forgetful algorithms. If points, then the decay factor
   * is raised to the power of number of new points and if batches, then decay factor will be
   * used as is.
   */
  val HALF_LIFE_UNIT: String = "spark.ml.kmeans.halfLife.unit"
}
