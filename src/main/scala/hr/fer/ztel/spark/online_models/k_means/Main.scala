package hr.fer.ztel.spark.online_models.k_means

import java.io.FileInputStream

import hr.fer.ztel.spark.online_models.k_means.configuration.{ConfigurationFields, ConfigurationProvider}
import hr.fer.ztel.spark.online_models.k_means.kafka.KafkaProducerWrapper
import org.apache.kafka.clients.consumer.ConsumerConfig
import org.apache.kafka.common.serialization.{StringDeserializer, StringSerializer}
import org.apache.log4j.{Level, Logger}
import org.apache.spark.broadcast.Broadcast
import org.apache.spark.mllib.clustering.StreamingKMeans
import org.apache.spark.mllib.regression.LabeledPoint
import org.apache.spark.streaming.dstream.DStream
import org.apache.spark.streaming.kafka010.{ConsumerStrategies, KafkaUtils, LocationStrategies}
import org.apache.spark.streaming.{Milliseconds, StreamingContext}
import org.apache.spark.{SparkConf, SparkContext}

object Main extends App {

  import ConfigurationFields._
  import ConsumerStrategies._
  import LocationStrategies._

  val configurations: ConfigurationProvider = loadConfig(args)

  Logger.getLogger("org").setLevel(Level.toLevel(configurations.get(LOGGING_LEVEL)))
  Logger.getLogger("akka").setLevel(Level.toLevel(configurations.get(LOGGING_LEVEL)))
  Logger.getRootLogger.setLevel(Level.toLevel(configurations.get(LOGGING_LEVEL)))

  val kafkaConsumerConfigurations = Map[String, Object](
    ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG -> configurations.get(STREAM_KAFKA_BROKERS),
    ConsumerConfig.GROUP_ID_CONFIG -> configurations.get(STREAM_KAFKA_GROUP_ID),
    ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG -> classOf[StringDeserializer].getName,
    ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG -> classOf[StringDeserializer].getName,
    ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG -> "false",
    ConsumerConfig.AUTO_OFFSET_RESET_CONFIG -> "latest"
  )

  val ssc = StreamingContext.getOrCreate(configurations.get(CHECKPOINT_DIR), () => createSSC())
  val sc = ssc.sparkContext
  ssc.start()

  val timeout = configurations.get(SPARK_STREAMING_TIMEOUT_SECONDS).toLong
  if (timeout > 0) {
    ssc.awaitTermination()
  } else {
    ssc.awaitTerminationOrTimeout(1000L * timeout)
  }

  def loadConfig(args: Array[String]): ConfigurationProvider = {
    val env = new ConfigurationProvider()

    if (args != null && args.length == 1) {
      env.load(new FileInputStream(args(0)))
    } else {
      env.load(new FileInputStream(CONFIG_FILE_DEFAULT_LOCATION))
    }

    env
  }

  def createSSC(): StreamingContext = {
    val sparkConfig = new SparkConf()
      .setAppName(configurations.get(SPARK_APP_NAME))
      .setMaster(configurations.get(SPARK_MASTER_URL))

    val sc = SparkContext.getOrCreate(sparkConfig)
    val ssc = new StreamingContext(sc, Milliseconds(configurations.get(SPARK_STREAMING_BATCH_TIME_MILLIS).toLong))

    val trainingDataStream = readLabeledDataStream(
      ssc = ssc,
      topics = getTopicsFromConfiguration(STREAM_KAFKA_TOPICS_TRAIN_DATA)
    ).map(x => x._2.features)
      .cache()

    val testDataStream = readLabeledDataStream(
      ssc = ssc,
      topics = getTopicsFromConfiguration(STREAM_KAFKA_TOPICS_TEST_DATA)
    ).mapValues(x => x.features)
      .cache()

    val model = new StreamingKMeans()
      .setK(configurations.get(NUM_OF_CLUSTERS).toInt)
      .setRandomCenters(configurations.get(NUMBER_OF_FEATURES).toInt, configurations.get(RANDOM_POINT_WEIGHT).toDouble)
      .setHalfLife(configurations.get(HALF_LIFE_VALUE).toDouble, configurations.get(HALF_LIFE_UNIT))

    model.trainOn(trainingDataStream)

    val kafkaProducer: Broadcast[KafkaProducerWrapper[String, String]] = {
      sc.broadcast(KafkaProducerWrapper(
        configurations.get(STREAM_KAFKA_BROKERS),
        configurations.get(STREAM_KAFKA_GROUP_ID),
        configurations.get(STREAM_KAFKA_TOPIC_OUTPUT),
        classOf[StringSerializer].getName,
        classOf[StringSerializer].getName
      ))
    }

    model.predictOnValues(testDataStream)
      .foreachRDD(rdd =>
        rdd.foreachPartition(partition =>
          partition.foreach { record =>
            kafkaProducer.value.send(record._1, record._2.toString)
          }
        )
      )

    ssc
  }

  def readLabeledDataStream(ssc: StreamingContext, topics: Array[String]): DStream[(String, LabeledPoint)] = {
    KafkaUtils.createDirectStream[String, String](
      ssc, PreferConsistent, Subscribe[String, String](topics, kafkaConsumerConfigurations)
    ).map(x => (x.key(), LabeledPoint.parse(x.value())))
  }

  def getTopicsFromConfiguration(topicsField: String): Array[String] = {
    configurations.get(topicsField)
      .replace(" ", "")
      .split(",")
  }
}