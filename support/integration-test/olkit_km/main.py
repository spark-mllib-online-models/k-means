import csv
import random
import sys
import time
from argparse import ArgumentParser, FileType, ArgumentTypeError

import matplotlib.pyplot as plt
import numpy as np
from kafka import KafkaProducer, KafkaConsumer
from sklearn.datasets import make_blobs
from sklearn.metrics import adjusted_rand_score, homogeneity_score, fowlkes_mallows_score
from sklearn.model_selection import train_test_split

APP_ID = random.randint(0, 100000)


def str2bool(v):
    if isinstance(v, bool):
        return v
    elif v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise ArgumentTypeError('Boolean value expected.')


def init_argparse():
    parser = ArgumentParser(prog='olkit-km',
                            description='Integration test for Apache Spark ML clustering models capable for online learning using Kafka topic as train/test stream. It generates multiple linearly separable batches of numeric data, splits it into train/test set and sends it through two different topics. After all batches, it reads from the output topic and then performs the model evaluation and data visualization.')

    parser.add_argument('-b', '--bootstrap_servers', nargs='*', default=['localhost:9092'],
                        help='List of Kafka bootstrap servers.')
    parser.add_argument('-trt', '--train_topic',
                        help='Kafka topic used as labeled (train) data stream. (default: "train_data")',
                        default='train_data')
    parser.add_argument('-tet', '--test_topic', help='Kafka topic used as test data stream. (default: "test_data")',
                        default='test_data')
    parser.add_argument('-ot', '--output_topic',
                        help='Kafka topic from which the model prediction will be read. (default: "output")',
                        default='output')
    parser.add_argument("-o", "--output_file", type=FileType("w"),
                        help="Output file, default: stdout.", default=sys.stdout)
    parser.add_argument('-ns', '--num_samples', type=int, default=100,
                        help='Number of samples per batch which will later be split into train and test set.')
    parser.add_argument('-tt', '--train_test', type=float, default=0.77, help='Train/test data ratio')
    parser.add_argument('-nf', '--num_features', type=int,
                        help='Number of features which will be generated in the sample. (default: 2)', default=2)
    parser.add_argument('-nif', '--num_redundant', type=int,
                        help='Number of redundant features. These features are generated as random linear combinations of the informative features. (default: 0)',
                        default=0)
    parser.add_argument('-nit', '--num_iterations', type=int,
                        help='Number of iterations/batches after which the model evaluation will be performed. (default: 10)',
                        default=10)
    parser.add_argument('-nc', '--num_clusters', type=int, help='Number of clusters', default=5)
    parser.add_argument('-cd', '--cluster_deviation', type=float,
                        help='Standard deviation around the blob centers. (default: 0.01)',
                        default=0.01)
    parser.add_argument('-m', '--magnitude', type=float,
                        help='Value which defines the magnitude of the translation for each center. (default: 0.1)',
                        default=0.1)
    parser.add_argument('-ct', '--consumer_timeout_ms', type=int, default=15000,
                        help='Number of millis to wait after each record consumed over output topic. (default: 15 sec)')
    parser.add_argument('-r', '--random_state', type=int, default=13, help='Random state')
    parser.add_argument('-v', '--visualize', type=str2bool, default=False,
                        help='Visualize datasets. This flag works only ')
    parser.add_argument('-sv', '--save_visualizations', type=str2bool, default=False,
                        help='Save visualizations as png.')

    return parser


def generate_centers(num_centers, num_features):
    return np.random.rand(num_centers, num_features)


def update_centers(centers, num_features, magnitude):
    new_centers = []

    for c in centers:
        new_centers.append(np.array(
            [c[i] + np.random.uniform(-magnitude, magnitude) for i in range(num_features)]))

    return np.array(new_centers)


def generate_data(args, random_state, centers):
    return make_blobs(n_samples=args.num_samples, centers=centers,
                      cluster_std=args.cluster_deviation, random_state=random_state)


def format_labeled_point(X, y):
    return f'({y},[{",".join((str(x) for x in X))}])'


def set_grid(ax, xx, yy):
    ax.set_xlim(xx.min(), xx.max())
    ax.set_ylim(yy.min(), yy.max())
    ax.set_xticks(())
    ax.set_yticks(())


def visualize_data(X, y, X_train, y_train, X_test, y_test, y_predicted, iteration):
    h = .2  # step size in the mesh
    x_min, x_max = X[:, 0].min() - .5, X[:, 0].max() + .5
    y_min, y_max = X[:, 1].min() - .5, X[:, 1].max() + .5
    xx, yy = np.meshgrid(np.arange(x_min, x_max, h), np.arange(y_min, y_max, h))

    v_train = plt.subplot(1, 2, 1)
    v_test = plt.subplot(1, 2, 2)

    # Plot the training points
    v_train.scatter(X_train[:, 0], X_train[:, 1], c=y_train, cmap='viridis', alpha=0.6)
    v_train.scatter(X_test[:, 0], X_test[:, 1], c=y_test, cmap='viridis', edgecolors='k')
    v_train.set_title(f"Train data - iteration {iteration}")

    # Plot the predicted points
    v_test.scatter(X_train[:, 0], X_train[:, 1], c=y_train, cmap='viridis', alpha=0.6)
    v_test.scatter(X_test[:, 0], X_test[:, 1], c=y_predicted, cmap='viridis', edgecolors='k')
    v_test.set_title(f"Predicted data - iteration {iteration}")

    set_grid(v_train, xx, yy)
    set_grid(v_test, xx, yy)


def send_data(X, y, producer, topic, iteration):
    index = 0

    for (curr_X, curr_y) in zip(X, y):
        producer.send(topic, key=f"{APP_ID}-{iteration}-{index}", value=format_labeled_point(curr_X, curr_y))
        index += 1

    return index


def receive_data(consumer, data):
    for message in consumer:
        parts = message.key.split("-")

        if int(parts[0]) != APP_ID:
            continue

        data[int(parts[1])]['y_predicted'].append((int(parts[2]), float(message.value)))


def create_producer(args):
    kwargs = {
        'bootstrap_servers': args.bootstrap_servers,
        'key_serializer': lambda x: x.encode('utf-8'),
        'value_serializer': lambda x: x.encode('utf-8')
    }

    return KafkaProducer(**kwargs)


def create_consumer(args):
    kwargs = {
        'bootstrap_servers': args.bootstrap_servers,
        'enable_auto_commit': True,
        'group_id': 'olkit-group',
        'key_deserializer': lambda x: x.decode('utf-8'),
        'value_deserializer': lambda x: x.decode('utf-8'),
        'consumer_timeout_ms': args.consumer_timeout_ms
    }

    return KafkaConsumer(args.output_topic, **kwargs)


def main():
    args = init_argparse().parse_args()

    plt.rcParams["figure.figsize"] = (15, 5)

    producer = create_producer(args)
    visualize = args.num_features == 2 and args.visualize

    writer = csv.DictWriter(args.output_file,
                            fieldnames=['adjusted_rand_score',
                                        'homogeneity_score',
                                        'fowlkes_mallows_score'])
    writer.writeheader()

    data = {}
    centers = generate_centers(args.num_clusters, args.num_features)

    for iteration in range(0, args.num_iterations):
        X, y = generate_data(args, args.random_state, centers)
        centers = update_centers(centers, args.num_features, args.magnitude)

        X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=args.train_test,
                                                            random_state=args.random_state)
        data[iteration] = {}
        data[iteration]['X'] = X
        data[iteration]['y'] = y
        data[iteration]['X_train'] = X_train
        data[iteration]['y_train'] = y_train
        data[iteration]['X_test'] = X_test
        data[iteration]['y_test'] = y_test
        data[iteration]['y_predicted'] = []

        send_data(X_train, y_train, producer, args.train_topic, iteration)
        time.sleep(5)
        send_data(X_test, y_test, producer, args.test_topic, iteration)
        time.sleep(5)

    receive_data(create_consumer(args), data)
    for iteration in range(args.num_iterations):
        X_test_eff = []
        y_test_eff = []
        y_predicted_eff = []

        X = data[iteration]['X']
        y = data[iteration]['y']
        X_train = data[iteration]['X_train']
        y_train = data[iteration]['y_train']
        X_test = data[iteration]['X_test']
        y_test = data[iteration]['y_test']
        y_predicted_tuples = data[iteration]['y_predicted']

        for (index, label) in y_predicted_tuples:
            X_test_eff.append(X_test[index])
            y_test_eff.append(y_test[index])
            y_predicted_eff.append(label)

        X_test = np.array(X_test_eff)
        y_test = np.array(y_test_eff)
        y_predicted = np.array(y_predicted_eff)

        score = {
            'adjusted_rand_score': adjusted_rand_score(y_test, y_predicted),
            'homogeneity_score': homogeneity_score(y_test, y_predicted),
            'fowlkes_mallows_score': fowlkes_mallows_score(y_test, y_predicted)
        }

        writer.writerow(score)

        if visualize:
            visualize_data(X, y, X_train, y_train, X_test, y_test, y_predicted, iteration)

            if args.save_visualizations:
                plt.savefig(f'{APP_ID}-{iteration}.png', bbox_inches='tight')
            else:
                plt.show()


if __name__ == '__main__':
    main()
