import setuptools

with open("requirements.txt", "r") as f:
    reqs = f.read().splitlines()

setuptools.setup(
    name='online-learning-kafka-integration-test-classification',
    version='0.0.1',
    author='Darko Britvec',
    author_email='darko.britvec@fer.hr',
    description='Integration test for Apache Spark ML models capable for online learning using Kafka topic as train/test stream.',
    packages=['olkit_km'],
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Developers',
        'Programming Language :: Python :: 3',
        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent',
    ],
    python_requires='>=3.6',
    keywords=['kafka', 'kafka-producer', 'kafka-consumer', 'scikit-learn'],
    entry_points={
        'console_scripts': ['olkit-c=olkit_km.main:main'],
    },
    install_requires=reqs,
    include_package_data=True
)
